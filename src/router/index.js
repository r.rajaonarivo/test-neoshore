import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Todo from '../views/Todo'
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

Vue.use(VueRouter)

const routes = [
  // Redirection to the homepage
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  // Redirection to the list of tasks
  {
    path: '/todo',
    name: 'Todo',
    component: Todo
  }
]

const router = new VueRouter({
  mode: 'history',
  base: '/',
  routes
})

export default router
